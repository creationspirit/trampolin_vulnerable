from django import forms

from vulnerable.models import User


class UploadFileForm(forms.Form):
    file = forms.FileField()

class UserForm(forms.ModelForm):
    """ User registration form """
    class Meta:
        model = User
        exclude = ['last_login', 'date_joined', 'is_active']