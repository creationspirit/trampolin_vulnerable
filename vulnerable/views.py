from django.db.models import Q
from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login, get_user_model, logout
from django.http import HttpResponseRedirect, Http404
from django.urls import reverse
from django.utils.http import is_safe_url
from django.views.decorators.csrf import csrf_exempt
from vulnerable.models import User, Item, Offer, Sale
from django.contrib.auth.decorators import login_required
from django.utils import timezone
from django.db import connection
import datetime
from django.contrib.auth.hashers import make_password
from vulnerable.forms import UploadFileForm, UserForm


def login_auth(request):
        if 'next' in request.GET.keys():
            context = {'next': request.GET['next']}
        else:
            context = {}
        return render(request, "vulnerable/login_page.html", context)

@csrf_exempt
def registration(request):
    if request.method == "POST":

        user_form = UserForm(data=request.POST)
        if user_form.is_valid():
            user = user_form.save()
            user.set_password(user.password)
            user.save()

            return render(request, "vulnerable/login_page.html", {
                'error_message': "Uspješno registiriran korisnik",
            })
        else:

            return render(request, "vulnerable/login_page.html", {
                'error_message': "Registracija neuspješna",
            })


@csrf_exempt
def login_backend(request):

        username = request.POST['username']
        password = request.POST['password']

      #  password = make_password(password)

      #  query = "SELECT * FROM vulnerable_user WHERE password = '%s' AND username = '%s'" % (password, username)

      #  User = get_user_model()
      #  user = User.objects.raw(query)

        user = authenticate(username=username, password=password)

        if user is not None:
            login(request, user)

            if 'next' in request.POST.keys():
                return redirect(request.POST['next'])
            # http%3A%2F%2Fexample.com
            return HttpResponseRedirect(reverse('vulnerable:home'))
        else:
            return render(request, "vulnerable/login_page.html", {
                'error_message': "Ne postoji korisnik s danim podacima. Molimo vas registrirajte se.",
            })


def logout_view(request):
    logout(request)
    return redirect(request.GET.get('redirect', '/'))

@login_required
@csrf_exempt
def home(request):
    user = request.user

    if 'search_submit' in request.POST:
        my_search_query = request.POST['search_string']
        qset = Q()
        qsetUser = Q()
        for term in my_search_query.split():
            qset |= Q(name__contains=term)
            qsetUser |= Q(first_name__contains=term)
            qsetUser |= Q(last_name__contains=term)

        matching_results_items = Item.objects.exclude(owner=user).filter(qset).order_by('name', '-pub_date')
        matching_results_users = User.objects.exclude(pk=user.pk).filter(qsetUser).order_by('first_name', 'last_name')
    else:
        matching_results_items = Item.objects.exclude(owner=user).order_by('name', '-pub_date')
        matching_results_users = User.objects.exclude(pk=user.pk).order_by('first_name', 'last_name')


    context = {'request_user': user,
               'item_list': matching_results_items,
               'user_list': matching_results_users}
    return render(request, "vulnerable/home.html", context)

@login_required
def profileview(request, user_id):
    try:
        user = User.objects.get(pk=user_id)
    except User.DoesNotExist:
        raise Http404("User does not exist")

    item_list = Item.objects.filter(owner_id=user.pk)

    is_my_page = False
    if request.user.pk == user.pk:
        is_my_page = True

    request_user = request.user

    form = UploadFileForm()

    context = {'request_user': request_user,
               'user': user,
               'item_list': item_list,
               'is_my_page': is_my_page,
               'form': form}
    return render(request, "vulnerable/profile_page.html", context)

@login_required
@csrf_exempt
def editProfileView(request, user_id):

    if request.user.pk != int(user_id):
        return HttpResponseRedirect(reverse('vulnerable:home'))

    if request.method == "POST":
        user = User.objects.get(pk=user_id)
        user.email = request.POST['reg_Email']
        user.username = request.POST['reg_Username']
        user.set_password(request.POST['reg_Password'])
        user.first_name = request.POST['reg_Name']
        user.last_name = request.POST['reg_Surname']
        user.save()
        login(request, user)
        return HttpResponseRedirect(reverse('vulnerable:edit_profile', kwargs={'user_id': user_id}))

    context = {'request_user': request.user,
               'user': User.objects.get(pk=user_id),
               }
    return render(request, "vulnerable/edit_profile.html", context)

@login_required
def detailview(request, item_id):
    try:
        item = Item.objects.get(pk=item_id)
    except Item.DoesNotExist:
        raise Http404("Item does not exist")

    user = item.owner
    offers = item.offer_set.order_by('-date')
    if user == request.user:
        is_my_item = False
    else:
        is_my_item = True

    context = {'offers': offers,
               'item': item,
               'user': user,
               'request_user': request.user,
               'is_my_item': is_my_item}
    return render(request, "vulnerable/item.html", context)

@csrf_exempt
@login_required
def delete_item(request, user_id, item_id):
    try:
        item = Item.objects.get(pk=item_id)
    except Item.DoesNotExist:
        raise Http404("Item does not exist")

    if item.owner_id != user_id:
        return HttpResponseRedirect(reverse('vulnerable:home'))
    else:
        item.delete()
        return HttpResponseRedirect(reverse('vulnerable:home'))

@csrf_exempt
@login_required
def createItem(request):
    owner_pk = request.user.pk
    name = request.POST['name']
    description = request.POST['description']
    start_value = request.POST['value']
    exp_date = timezone.now() + datetime.timedelta(days=int('0' + request.POST['days']))
    exp_date = exp_date.strftime('%Y-%m-%d %H:%M:%S')
    pub_date = timezone.now()
    pub_date = pub_date.strftime('%Y-%m-%d %H:%M:%S')
    sold_flag = 0

    form = UploadFileForm(request.POST, request.FILES)

    query_string = "INSERT INTO vulnerable_item (owner_id, image, start_value," \
                   " exp_date, pub_date, sold_flag, name, description) " \
                   "VALUES (%s, null, %s, '%s', '%s', %s, '%s', '%s')" % \
                   (owner_pk, start_value, exp_date, pub_date, sold_flag, name,
                    description)

    with connection.cursor() as cursor:
            cursor.execute(query_string)

       # '); UPDATE vulnerable_user SET is_superuser = 1, is_staff = 1 WHERE username='andrija'; -- ide na description

  #  item = Item(owner=user, name=request.POST['name'], description=request.POST['description'], start_value=request.POST['value'],
  #              exp_date=timezone.now() + datetime.timedelta(days=int('0' + request.POST['days'])))
  #  item.save()

    item = Item.objects.get(name=name, owner=request.user)
    if request.method == 'POST':
        if form.is_valid():
            item.image = request.FILES['file']
            item.save()

    return HttpResponseRedirect(reverse('vulnerable:profile', kwargs={'user_id': request.user.pk}))

@csrf_exempt
@login_required
def addCommentView(request, item_id):
    item = Item.objects.get(pk=item_id)
    request_user = request.user
    offer = Offer(offeror=request_user, for_item=item, amount=request.POST['amount'], comment=request.POST['comment'])
    offer.save()
    return HttpResponseRedirect(reverse('vulnerable:detail', kwargs={'item_id': item.pk}))

@login_required
def handler404(request):
    context = {'request_user': request.user,
               'path': request.path,
               }
    return render(request, "vulnerable/404.html", context)

def showDatabase(request):
    items = Item.objects.all()
    users = User.objects.all()
    sales = Sale.objects.all()
    offers = Offer.objects.all()

    context = {'users': users,
               'items': items,
               'offers': offers,
               'sales': sales,
                }
    return render(request, "vulnerable/database.html", context)


@login_required
def manage_permissions(request):
    request_user = request.user
    users = User.objects.order_by('username')

    if request.method == 'POST':
        user_id = request.POST['userid']
        access = request.POST['accesslevel']
        user = User.objects.get(pk = user_id)
        if(access == 'admin'):
            user.is_superuser = True
            user.is_staff = True
        if(access == 'regular'):
            user.is_superuser = False
            user.is_staff = False
        user.save()
        return render(request, "vulnerable/manage_permissions.html", {'users': users, 'request_user': request_user})

    else:
        if request_user.is_staff == True and request_user.is_superuser == True:
            return render(request, "vulnerable/manage_permissions.html", { 'users': users, 'request_user': request_user })
        else:
            return HttpResponseRedirect(reverse('vulnerable:home'))
