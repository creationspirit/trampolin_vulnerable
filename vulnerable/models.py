from django.db import models
from django.contrib.auth.models import AbstractUser
from django.utils import timezone
import datetime

# Create your models here.

class User(AbstractUser):
    pass

User._meta.get_field('first_name').max_length = 1000

def get_image_path(instance, filename):
    return 'item_{0}/{1}'.format(instance.item_id, filename)

class Item(models.Model):
    item_id = models.AutoField(primary_key=True)
    image = models.FileField(upload_to=get_image_path, blank=True, null=True, default=None)
    owner = models.ForeignKey(User, on_delete=models.CASCADE)
    name = models.CharField(max_length=100)
    description = models.TextField()
    start_value = models.FloatField(default=0)
    pub_date = models.DateTimeField('date published', default=timezone.now)
    exp_date = models.DateTimeField('expiration date', default=timezone.now() + datetime.timedelta(days=30))
    sold_flag = models.IntegerField(default=0)

class Offer(models.Model):
    offer_id = models.AutoField(primary_key=True)
    offeror = models.ForeignKey(User, on_delete=models.CASCADE)
    for_item = models.ForeignKey(Item, on_delete=models.CASCADE)
    amount = models.FloatField(default=0)
    comment = models.TextField()
    date = models.DateTimeField('date offered', default=timezone.now)

class Sale(models.Model):
    sale_id = models.AutoField(primary_key=True)
    buyer = models.ForeignKey(User, on_delete=models.CASCADE)
    item = models.ForeignKey(Item, on_delete=models.CASCADE)
    amount = models.FloatField()


