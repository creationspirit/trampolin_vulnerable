from django.conf.urls import url

from . import views


urlpatterns = [
    url(r'^$', views.login_auth, name='login'),
    url(r'^login/$', views.login_backend, name='login_backend'),
    url(r'^register/$', views.registration, name='registration'),
    url(r'^home/$', views.home, name='home'),
    url(r'^profile/(?P<user_id>[0-9]+)/$', views.profileview, name='profile'),
    url(r'^details/(?P<item_id>[0-9]+)/$', views.detailview, name='detail'),
    url(r'^logout/$', views.logout_view, name='logout'),
    url(r'^createitem/$', views.createItem, name='create_item'),
    url(r'^addoffer/(?P<item_id>[0-9]+)/$', views.addCommentView, name='offer'),
    url(r'^delete_item/(?P<user_id>[0-9]+)/(?P<item_id>[0-9]+)/$', views.delete_item, name='delete_item'),
    url(r'^edit_profile/(?P<user_id>[0-9]+)/$', views.editProfileView, name='edit_profile'),
    url(r'^dev/db/$', views.showDatabase, name='database'),
    url(r'^manage_permissions/$', views.manage_permissions, name='manage_permissions'),


]